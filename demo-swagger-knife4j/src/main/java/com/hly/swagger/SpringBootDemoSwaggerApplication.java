package com.hly.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @ClassName: SpringBootDemoSwaggerApplication
 * @Description: 启动器
 * @author: He LongYun
 * @date: 2022年6月12日 下午7:21:21
 */
@SpringBootApplication
public class SpringBootDemoSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoSwaggerApplication.class, args);
    }
}
