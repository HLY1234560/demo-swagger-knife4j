package com.hly.swagger.common;

/**
 * 
 * @ClassName: DataType
 * @Description: 方便在 @ApiImplicitParam 的 dataType 属性使用
 * @author: He LongYun
 * @date: 2022年6月12日 下午7:19:44
 */
public final class DataType {

    public final static String STRING = "String";
    public final static String INT = "int";
    public final static String LONG = "long";
    public final static String DOUBLE = "double";
    public final static String FLOAT = "float";
    public final static String BYTE = "byte";
    public final static String BOOLEAN = "boolean";
    public final static String ARRAY = "array";
    public final static String BINARY = "binary";
    public final static String DATETIME = "dateTime";
    public final static String PASSWORD = "password";

}
