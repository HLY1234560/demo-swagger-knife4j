package com.hly.swagger.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @ClassName: User
 * @Description: 用户实体
 * @author: He LongYun
 * @date: 2022年6月12日 下午7:21:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户实体", description = "用户实体-用于返回用户信息")
public class User implements Serializable {
    private static final long serialVersionUID = 5057954049311281252L;
    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id", required = true, example = "1")
    private Integer id;
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名", required = true, example = "何龙云")
    private String name;
    /**
     * 工作岗位
     */
    @ApiModelProperty(value = "工作岗位", required = true, example = "java开发工程师")
    private String job;
}
