package com.hly.swagger.controller;

import com.hly.swagger.common.ApiResponse;
import com.hly.swagger.common.DataType;
import com.hly.swagger.common.ParamType;
import com.hly.swagger.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName: UserController
 * @Description: 测试Controller
 * @author: He LongYun
 * @date: 2022年6月12日 下午7:20:22
 */
@RestController
@RequestMapping("/user")
@Api(tags = "1.0.0-SNAPSHOT", description = "用户管理", value = "用户管理")
@Slf4j
public class UserController {

    @GetMapping("/getByUserName")
    @ApiOperation(value = "条件查询（DONE）", notes = "备注")
    @ApiImplicitParams({@ApiImplicitParam(name = "username", value = "用户名", dataType = DataType.STRING, paramType = ParamType.QUERY, defaultValue = "xxx")})
    public ApiResponse<User> getByUserName(@RequestParam("username") String username) {
        log.info("多个参数用  @ApiImplicitParams");
        return ApiResponse.<User>builder().code(200).message("操作成功").data(new User(1, username, "JAVA")).build();
    }

    @GetMapping("/getById")
    @ApiOperation(value = "主键查询（DONE）", notes = "备注")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户编号", dataType = DataType.INT, paramType = ParamType.QUERY)})
    public ApiResponse<User> getById(@RequestParam("id") Integer id) {
        log.info("单个参数用  @ApiImplicitParam");
        return ApiResponse.<User>builder().code(200).message("操作成功").data(new User(id, "u1", "p1")).build();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(value = "删除用户（DONE）", notes = "备注")
    @ApiImplicitParam(name = "id", value = "用户编号", dataType = DataType.INT, paramType = ParamType.QUERY)
    public ApiResponse<User> deleteById(@RequestParam("id") Integer id) {
        log.info("单个参数用 ApiImplicitParam");
        return ApiResponse.<User>builder().code(200).message("操作成功").data(new User(id, "u1", "p1")).build();
    }

    @PostMapping("/save")
    @ApiOperation(value = "添加用户（DONE）")
    public ApiResponse<User> save(@RequestBody User user) {
        log.info("如果是 POST PUT 这种带 @RequestBody 的可以不用写 @ApiImplicitParam");

        return ApiResponse.<User>builder().code(200).message("操作成功").data(user).build();
    }

    @PostMapping("/multipar")
    @ApiOperation(value = "添加用户（DONE）")
    public ApiResponse<List<User>> multipar(@RequestBody List<User> user) {
        log.info("如果是 POST PUT 这种带 @RequestBody 的可以不用写 @ApiImplicitParam");

        return ApiResponse.<List<User>>builder().code(200).message("操作成功").data(user).build();

    }

    @PostMapping("/array")
    @ApiOperation(value = "添加用户（DONE）")
    public ApiResponse<User[]> array(@RequestBody User[] user) {
        log.info("如果是 POST PUT 这种带 @RequestBody 的可以不用写 @ApiImplicitParam");

        return ApiResponse.<User[]>builder().code(200).message("操作成功").data(user).build();

    }

    @PutMapping("/put")
    @ApiOperation(value = "修改用户（DONE）")
    public void put(@RequestParam("id") Integer id, @RequestBody User user) {
        log.info("如果你不想写 @ApiImplicitParam 那么 swagger 也会使用默认的参数名作为描述信息 ");


    }

    @PostMapping("/{id}/file")
    @ApiOperation(value = "文件上传（DONE）")
    public String file(@PathVariable Long id, @RequestParam("file") MultipartFile file) {
        log.info(file.getContentType());
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        return file.getOriginalFilename();
    }
}
