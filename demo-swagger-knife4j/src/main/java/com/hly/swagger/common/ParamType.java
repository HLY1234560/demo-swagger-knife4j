package com.hly.swagger.common;

/**
 * 
 * @ClassName: ParamType
 * @Description: 方便在 @ApiImplicitParam 的 paramType 属性使用
 * @author: He LongYun
 * @date: 2022年6月12日 下午7:19:54
 */
public final class ParamType {

    public final static String QUERY = "query";
    public final static String HEADER = "header";
    public final static String PATH = "path";
    public final static String BODY = "body";
    public final static String FORM = "form";

}
